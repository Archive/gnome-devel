#!/bin/sh

function doexit {
	echo "Build of $1 failed."
	exit 1
}

function dowarn {
	echo "Build of $1 failed."
}

GNOME_PREFIX="$1"
if [ -z "$GNOME_PREFIX" ]; then
	for I in /usr/local /opt /opt/gnome /usr; do
		if [ -f $I/lib/gnomeConf.sh ]; then
			GNOME_PREFIX="$I"
			break
		fi
	done
fi

if [ -z "$GNOME_PREFIX" ]; then
	echo "Specify the prefix you want to use for gnome"
	echo "Example: ./build-gnome.sh /usr/local"
	echo '	will install all the gnome components under /usr/local/{bin,lib,share}'
	exit 1
fi

[ ! -d gnome-libs -a -d ../gnome-libs ] && cd ..

DIRS="`ls -d gnome-*/ | grep -v gnome-libs`"

for I in gtk+ gnome-libs; do
	echo "Building $I"
	[ -f $I/Makefile ] \
		|| (cd $I && ./autogen.sh --prefix=$GNOME_PREFIX) \
		|| doexit $I

	make -C $I || doexit $I

	if [ -w $GNOME_PREFIX/bin ]; then
		make -C $I install
	else
		echo "Can't install into $GNOME_PREFIX - please run:"
		echo "	make -C `pwd`/$I"
		echo "as root"
		echo
		echo "Press any key to continue building..."
		read
	fi
done

for I in $DIRS; do
	echo "Building $I"
	[ -f $I/Makefile ] \
		|| (cd $I && ./autogen.sh --prefix=$GNOME_PREFIX) \
		|| dowarn

	make -C $I || dowarn
	if [ -w $GNOME_PREFIX/bin ]; then
		make -C $I install
	else
		echo "Can't install into $GNOME_PREFIX - please run:"
		echo "	make -C `pwd`/$I"
		echo "as root"
		echo
		echo "Press any key to continue building..."
		read
	fi
done
