#include "glibc-mdebug.h"
#include <glib.h>

#ifdef __GLIBC__

static void my_malloc_initialize_hook(void);
static void my_free_hook(__malloc_ptr_t __ptr);
static __malloc_ptr_t my_malloc_hook(size_t __size);
static __malloc_ptr_t my_realloc_hook(__malloc_ptr_t __ptr,
				      size_t __size);
static __malloc_ptr_t my_memalign_hook(size_t __size,
				       size_t __alignment);
static void my_after_morecore_hook(void);

static void (*old_malloc_initialize_hook) __MALLOC_P ((void));
static void (*old_free_hook) __MALLOC_P ((__malloc_ptr_t __ptr));
static __malloc_ptr_t (*old_malloc_hook) __MALLOC_P ((size_t __size));
static __malloc_ptr_t (*old_realloc_hook) __MALLOC_P ((__malloc_ptr_t __ptr,
						size_t __size));
static __malloc_ptr_t (*old_memalign_hook) __MALLOC_P ((size_t __size,
						 size_t __alignment));
static void (*old_after_morecore_hook) __MALLOC_P ((void));

void init_mdebug(void)
{
  old_malloc_initialize_hook = __malloc_initialize_hook;
  old_free_hook = __free_hook;
  old_malloc_hook = __malloc_hook;
  old_realloc_hook = __realloc_hook;
  old_memalign_hook = __memalign_hook;
  old_after_morecore_hook = __after_morecore_hook;

  __malloc_hook = my_malloc_hook;
  __realloc_hook = my_realloc_hook;
  __free_hook = my_free_hook;
}

static void
my_free_hook(__malloc_ptr_t __ptr)
{
  static gboolean locked = FALSE;

  if(!locked)
    {
      locked = TRUE;
      g_print("%#x: free\n", __ptr);
      locked = FALSE;
    }

  if(old_free_hook)
    old_free_hook(__ptr);
}

static __malloc_ptr_t
my_malloc_hook(size_t __size)
{
  static gboolean locked = FALSE;
  __malloc_ptr_t return_val;

  return_val = old_malloc_hook(__size);

  if(!locked)
    {
      locked = TRUE;
      g_print("%#x: malloc %d\n", return_val, __size);
      locked = FALSE;
    }

  return return_val;
}

static __malloc_ptr_t
my_realloc_hook(__malloc_ptr_t __ptr,
		size_t __size)
{
  static gboolean locked = FALSE;
  __malloc_ptr_t return_val;

  return_val = old_realloc_hook(__ptr, __size);

  if(!locked)
    {
      locked = TRUE;
      g_print("%#x: realloc %#x to %d\n", return_val, __ptr, __size);
      locked = FALSE;
    }

  return return_val;
}

#endif /* __GLIBC__ */
