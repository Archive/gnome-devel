#include "checkmem.h"
#ifdef __OBJC__
#include <objc/Object.h>
#endif

#define MAX_ALLOC 4097

#ifdef __cplusplus
class testclass {
public:
	testclass(void) {}
	int aval;
};
#endif

void domemcheck(void)
{
  int i;
  char *tptr[MAX_ALLOC];
  void *mcptr[MAX_ALLOC];
#ifdef __OBJC__
  id iptr[MAX_ALLOC];
#endif
#ifdef __cplusplus
  testclass *optr[MAX_ALLOC];
#endif
  g_print("domemcheck()\n");
  for(i = 1; i < MAX_ALLOC; i++)
    {
#ifdef __OBJC__
      iptr[i] = [Object new];
#endif
#ifdef __cplusplus
      optr[i] = new testclass();
#endif
      tptr[i] = (char *)g_malloc0(i);
      mcptr[i] = g_string_new("hi there");
    }
  for(i = 1; i < MAX_ALLOC; i++)
    {
#ifdef __OBJC__
      [iptr[i] free];
#endif
#ifdef __cplusplus
      delete optr[i];
#endif
      g_free(tptr[i]);
      g_string_free(mcptr[i], TRUE);
    }
}
