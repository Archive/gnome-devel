#!/usr/bin/perl
# Caveats - currently breaks if more than one <chapter> is on a line, but
# I don't think that should be a problem ;-)
printf "%-30s %s\n", "index.html", "Manual";
while($aline = <>) {
	if($aline =~ /<chapter\s+id="([^"]+)"\s*>/i) {
		$thissect = $1;
		$prtsect = 1;
	}
	if($prtsect && $aline =~ /<title>([^<]+)<\/title>/i) {
		printf "%-30s %s\n", "index.html#".$thissect, $1;
		$prtsect = 0;
	}
}
